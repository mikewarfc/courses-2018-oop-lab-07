package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

//	private static final int CASSANI_AGE = 53;
//	private static final int ECCLESTONE_AGE = 83;
	private static final int ALONSO_AGE = 34;
	
    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
    	
//    	final SportSocialNetworkUserImpl<User> dcassani = new SportSocialNetworkUserImpl<>("Davide", "Cassani", "dcassani", CASSANI_AGE);
//    	final SportSocialNetworkUserImpl<User> becclestone = new SportSocialNetworkUserImpl<>("Bernie", "Ecclestone", "becclestone", ECCLESTONE_AGE);
    	final SportSocialNetworkUserImpl<User> falonso = new SportSocialNetworkUserImpl<>("Fernando", "Alonso", "falonso", ALONSO_AGE);
    	
    	falonso.addSport(Sport.F1);
    	falonso.addSport(Sport.BIKE);
    	falonso.addSport(Sport.SOCCER);
    	
    	System.out.println("Alonso practises F1: " + falonso.hasSport(Sport.F1));
    	System.out.println("Alonso does not like volley: " + !falonso.hasSport(Sport.VOLLEY));
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    }

}
