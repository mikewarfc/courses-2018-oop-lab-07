package it.unibo.oop.lab.nesting2;

import java.util.Iterator;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	private final List<T> toBeAccepted;
	
	public OneListAcceptable(final List<T> list) {
		this.toBeAccepted = list;
	}

	@Override
	public Acceptor<T> acceptor() {
		final Iterator<T> iterator = toBeAccepted.iterator();
		return new Acceptor<T>() {
						
			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if (iterator.hasNext() && newElement.equals(iterator.next())) {
					return;
				} else {
					throw new ElementNotAcceptedException(newElement);
				}
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if (iterator.hasNext()) {
					throw new EndNotAcceptedException();
				}
			}
		};
	}

}
