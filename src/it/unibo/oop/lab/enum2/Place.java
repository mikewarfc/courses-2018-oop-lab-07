package it.unibo.oop.lab.enum2;

/**
 * 
 * @author miche
 * 
 * An Enumeration defining if something is to be practiced indoor or outdoor
 *
 */
public enum Place {
	INDOOR, OUTDOOR;
}
