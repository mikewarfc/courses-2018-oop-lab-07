/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {
	BASKET(Place.INDOOR, "Basket", 5),
	VOLLEY(Place.INDOOR, "Volley", 6),
	TENNIS(Place.OUTDOOR, "Tennis", 1),
	BIKE(Place.OUTDOOR, "Bike", 1),
	F1(Place.OUTDOOR, "F1", 1),
	MOTOGP(Place.OUTDOOR, "MotoGP", 1),
	SOCCER(Place.OUTDOOR, "Soccer", 11);

	private final int noTeamMembers;
	private final String sportName;
	private final Place place;

	/**
	 * Constructor for a sport
	 * 
	 * @param place
	 * 		if it is practiced indoor or outdoor
	 * @param name
	 * 		the name of the sport
	 * @param noTeamMembers
	 * 		the number of members in a team for this sport
	 */
	private Sport(final Place place, final String name, final int noTeamMembers) {
		this.sportName = name;
		this.place = place;
		this.noTeamMembers = noTeamMembers;
	}
	
	/**
	 * @return true
	 * 		if the sport is an individual one
	 */
	public boolean isIndividualSport() {
		return this.noTeamMembers == 1;
	}
	
	/**
	 * @return true
	 * 		if the sport is an indoor one
	 */
	public boolean isIndoorSport() {
		return this.place.equals(Place.OUTDOOR);
	}
	
	/**
	 * @return the place where this sport is practiced
	 */
	public Place getPlace() {
		return this.place;
	}
	
	public String toString() {
		return this.sportName + "is practiced" + this.place + "by a team of " + this.noTeamMembers + 
				"people.";
	}
	
}
